package com.technocrats;


import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.assertj.core.api.Assertions.assertThat;

public class FutureCircuitBreakerTest extends AbstractIntegrationTest {

	@Test
	public void produceSuccess() throws InterruptedException {
		for (int i = 0; i < 10; i++) {
new Thread(() -> {
	ResponseEntity<String> response = restTemplate.getForEntity("/backend/futureSuccess", String.class);
	System.out.println(response.getStatusCode());
	assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
}).start();
		}

		Thread.sleep(10000);
	}
}
