package com.technocrats.controller;

import com.technocrats.service.BackendService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import java.util.concurrent.CompletableFuture;

@RestController
@RequestMapping(value = "/backend")
public class BackendController {

    private final BackendService businessService;

    public BackendController(@Qualifier("backendService") BackendService businessService){
        this.businessService = businessService;
    }

    @GetMapping("futureSuccess")
    public CompletableFuture<String> futureSuccess() throws InterruptedException {
        return businessService.futureSuccess();
    }
}
