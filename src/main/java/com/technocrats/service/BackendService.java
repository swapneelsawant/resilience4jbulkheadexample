package com.technocrats.service;

import io.github.resilience4j.bulkhead.annotation.Bulkhead;

import io.github.resilience4j.timelimiter.annotation.TimeLimiter;
import org.springframework.stereotype.Component;
import java.util.concurrent.CompletableFuture;

import static io.github.resilience4j.bulkhead.annotation.Bulkhead.Type;

/**
 * This Service shows how to use the CircuitBreaker annotation.
 */
@Component(value = "backendService")
public class BackendService  {

    private static final String BACKEND = "backend";

    @Bulkhead(name = BACKEND, type = Type.THREADPOOL)
    @TimeLimiter(name = BACKEND)
    public CompletableFuture<String> futureSuccess() throws InterruptedException {
       // Thread.sleep(2000);
        return CompletableFuture.completedFuture("Hello World from backend");
    }

}
